client = require('./lib/client')
api = new client('localhost', 2384)

cb = function (err, result, opts) {
	console.log(opts.method, opts.path, err || result)
}
breaker = function(test, cb) {
	return function (err) {
		if (err != null) {
			console.error('FAIL', test, err)
			throw err;
		}
		cb.apply(this, arguments)
	}
}

async = require('async')

api.setKey({
	"public": "d405b2a277758f906bdd30c75e7355c3",
	"private": "78760597ef5e5594faf0b1c2f16c7cb518b427a13e021d36413b4790181fe59b3daa3e1ea1cf26ec89a49d5014808af087f7cb2018dc76a35663d269a04c9064"
})

api.setKey({
	"public": "d405b2a277758f90737b51d7d5b26b3c"
})

user = {
	username: 'rich',
	email: 'richthegeek@gmail.com',
	password: 'password'
}

token = '$2a$12$a-ZJqJt/dd-j9IPahlq-s1QGOcpY'
api.setToken(token)

api.user.findToken(cb)
// api.user.updateToken({thing: 'thang'}, cb)

// process.exit(0)

// // create an account
// api.account.create(breaker('Account creation', function(err, account) {
// 	account = account.account
// 	api.setKey(account.keys.primary)

// 	console.log('Account created', account._id)

// 	user = {
// 		username: 'richard',
// 		email: 'richthegeek@gmail.com',
// 		password: 'password'
// 	}

// 	steps = {
// 		'Account read': function(next) {
// 			api.account.get(function(err, res) {
// 				next(err, 'OK')
// 			})
// 		},

// 		'Account update': function(next) {
// 			body = {field_options: {active: {'default': false}}}
// 			api.account.update(body, function(err, updated) {
// 				if (!err && updated.account.field_options.active['default'] != false) {
// 					err = err || 'Account update failed, no change registered.'
// 				}
// 				next(err, updated)
// 			});
// 		},

// 		'Account update key': function(next) {
// 			api.account.add_key('testing', function(err, res) {
// 				if (res.status == 'ok' && res.key.name == 'testing' && res.key.parent == 'primary') {
// 					next(null, res)
// 				} else{
// 					return next('Key did not have corret name and/or parent!', res)
// 				}
// 			})
// 		},

// 		'Account remove key': function(next) {
// 			api.account.remove_key('testing', next)
// 		},

// 		'User creation - empty': function(next) {
// 			api.users.create({}, function(err, result) {
// 				if (result.status == 'error') {
// 					return next(null, 'Empty account returned error as expected')
// 				}
// 				return next('Empty account DID NOT return an error, it really should')
// 			})
// 		},

// 		'User creation - correct': function(next) {
// 			api.users.create(user, next)
// 		},

// 		'Login': function(next) {
// 			api.auth.login(user, function(err, result) {
// 				if (result && result.status == 'ok' && result.token) {
// 					api.setToken(result.token);
// 				}
// 				next(err, result)
// 			})
// 		},

// 		'User get with token': function(next) {
// 			api.user.findToken(function(err, found) {
// 				if (user.username != found.username) {
// 					err = err || 'The usernames did not match!'
// 				}
// 				next(err, 'OK')
// 			})
// 		},

// 		'User update with token': function(next) {
// 			api.user.updateToken({email: 'bob@loblaw.com'}, next)
// 		},

// 		'Logout': function(next) {
// 			api.auth.logoutToken(next)
// 		},

// 		'Logout verify': function(next) {
// 			api.user.findToken(function(err, found) {
// 				if (err) {
// 					return next(null, err + ' (expected)')
// 				}
// 				next('Token was still active')
// 			});
// 		},

// 		'Account delete': function(next) {
// 			api.account.remove(next)
// 		},
// 	}
// 	async.series(steps, function(err, results) {
// 		if (err) {
// 			throw err
// 		}
// 		for (var k in results) {
// 			result = ([]).concat(results[k]).shift();
// 			if (result && result.statusText) {
// 				result = result.statusText;
// 			}
// 			console.log(k, ':', result)
// 		}
// 		process.exit(0)
// 	});

// }));
