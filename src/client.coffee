class AuthIOClient

	constructor: (@host = 'auth.trakapo.com', @port = 80, http = false) ->
		@angularHTTP = AuthIOClient.angularHTTP or http
		@key = {}
		@account = new AuthIOClient.Account @
		@user = @users = new AuthIOClient.User @
		@auth = new AuthIOClient.Auth @

	setKey: (@key) -> @
	setToken: (@token) -> @

	make_hash: (path, body, key = @key) ->
		hash_parts = [
			path,
			JSON.stringify(body or {}),
			key.private
		]
		return @_encrypt hash_parts.join ''

	_encrypt: (str) ->
		return require('crypto').createHash('sha256').update(str).digest('hex')

	_send: (method, path, options, callback) ->
		body = options.body or {}
		queryObj = options.query or {}

		if path.join
			path = '/' + path.join '/'

		if options.auth is 'key'
			if not @key
				return callback 'This route is HMAC-authorised, but no keypair has been provided'

			queryObj.key = @key.public
			if @key.private and @key.secure isnt false
				queryObj.hash = @make_hash path, body

		else if options.auth is 'token'
			if not @token
				return callback 'This route is token-authorised, but no token has been provided'
			queryObj.token = @token

		query = (encodeURIComponent(k) + '=' + encodeURIComponent(v) for k, v of queryObj)

		opts =
			host: @host,
			port: @port,
			path: path + '?' + query.join('&')
			method: method

		body = (body and JSON.stringify(body)) or false

		@_request opts, body, (err, res, data) ->
			try
				if err then throw err

				if typeof data is 'string'
					json = JSON.parse(data)
					if json.code
						throw json.message
				else
					json = data

				if 0 isnt Math.floor (res.statusCode - 200) / 200
					throw 'Statuscode wasnt 2xx'

				return callback null, json, opts
			catch e
				return callback e, data, opts

	_request: (options, data, callback) ->
		if typeof @angularHTTP is 'function'
			opts = {}
			url = "#{options.host}:#{options.port}#{options.path}"

			if data and options.method in ['POST', 'PUT']
				opts = data

			req = @angularHTTP[options.method.toLowerCase( )]( url, opts )
				.success ( data, status, headers, config ) ->
					callback null, { statusCode: status }, data

				.error ( data, status, headers, config ) ->
					callback data, { statusCode: status }, data
		else
			http = require 'http'
			req = http.request options, (res) ->
				data = ''
				res.on 'data', (chunk) -> data += chunk

				cb = (a, b, c) ->
					callback a, b, c
					cb = () -> null

				res.on 'end', () -> cb null, res, data

				res.on 'error', (err) -> return cb error

			req.write data if data and method in ['POST', 'PUT']
			req.end()

			return req

# Module Definitions
# Node
if module?
	AuthIOClient.Account = require './account'
	AuthIOClient.Auth = require './auth'
	AuthIOClient.User = require './user'
	module.exports = AuthIOClient

# AMD/RequireJS
if define?
	define 'authioClient', [
		'authioClient-account'
		'authioClient-auth'
		'authioClient-user'
	], (Account, Auth, User) ->
		AuthIOClient.Account = Account
		AuthIOClient.Auth = Auth
		AuthIOClient.User = User
		AuthIOClient

# Angular
if angular?
    angular.module('AuthIOClient', ['ng', 'authio.client.account', 'authio.client.auth', 'authio.client.user', 'authio.client.crypto']).
      factory('$authio', ($http, AuthIOAccount, AuthIOAuth, AuthIOUser, SHA256) ->
        AuthIOClient.angularHTTP = $http
        AuthIOClient.Account = AuthIOAccount
        AuthIOClient.Auth = AuthIOAuth
        AuthIOClient.User = AuthIOUser
        AuthIOClient.prototype._encrypt = SHA256
        return AuthIOClient
    )