class AuthIOAuth

	constructor: (@api) ->
		null

	login: (data, callback) ->
		args = Array::slice.call(arguments)
		callback = args.pop()
		data = args.pop()

		@api._send 'POST', '/login', {body: data, auth: 'key'}, callback

	logout: (callback) ->
		@api._send 'GET', '/logout', {auth: 'key'}, callback

	logoutToken: (callback) ->
		@api._send 'GET', '/logout', {auth: 'token'}, callback

	heartbeart: (callback) ->
		@api._send 'GET', '/heartbeart', {auth: 'token'}, callback

# Module Definitions
# Node
if module?
	module.exports = AuthIOAuth

# AMD/RequireJS
if define?
	define 'authioClient-auth', [], -> AuthIOAuth

if angular?
	angular.module('authio.client.auth', []).factory 'AuthIOAuth', -> AuthIOAuth
