class AuthIOUser

	constructor: (@api) ->
		null

	create: (user, callback) ->
		args = Array::slice.call(arguments)
		callback = args.pop()
		user = args.pop()

		@api._send 'POST', '/users', {body: user, auth: 'key'}, callback

	update: (username, user, callback) ->
		args = Array::slice.call(arguments)
		callback = args.pop()
		username = args.shift()
		user = args.pop()

		if username is 'me'
			return @updateToken user, callback

		@api._send 'POST', '/users/' + username, {body: user, auth: 'key'}, callback

	updateToken: (user, callback) ->
		callback = Array::pop.call arguments
		user = Array::pop.call arguments

		@api._send 'POST', '/users/me', {body: user, auth: 'token'}, callback

	remove: (username, callback) ->
		if username is 'me'
			return @removeToken callback
		@api._send 'DELETE', '/users/' + username, {auth: 'key'}, callback

	removeToken: (callback) ->
		@api._send 'DELETE', '/users/me', {auth: 'token'}, callback

	find: (query, options, callback) ->
		options.sort ?= {}
		if typeof options.sort isnt 'string'
			str = []
			for col, dir of options.sort
				str.push (dir > 0 and '' or '-') + col
			options.sort = str.join ','

		options.query ?= {}

		options.query.sort ?= options.sort
		options.query.page ?= options.page
		options.query.limit ?= options.limit

		options.body = query
		options.auth = 'token'

		@api._send 'GET', '/users', options, callback

	findBy: (column, value, callback) ->
		path = '/users/by-' + column + '/' + value
		@api._send 'GET', path, {auth: 'key'}, callback

	findToken: (callback) ->
		@api._send 'GET', '/users/me', {auth: 'token'}, callback


# Module Definitions
# Node
if module?
	module.exports = AuthIOUser

# AMD/RequireJS
if define?
	define 'authioClient-user', [], -> AuthIOUser

# Angular
if angular?
	angular.module('authio.client.user', []).factory 'AuthIOUser', -> AuthIOUser
