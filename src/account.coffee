class AuthIOAccount

	constructor: (@api) ->
		null

	create: (options, callback) ->
		args = Array::slice.call(arguments)
		callback = args.pop()
		options = args.pop()

		@api._send 'POST', '/register', {body: options}, callback

	get: (callback) ->
		@api._send 'GET', '/account', {auth: 'key'}, callback

	update: (options, callback) ->
		args = Array::slice.call(arguments)
		callback = args.pop()
		options = args.pop() or {}

		@api._send 'POST', '/account', {body: options, auth: 'key'}, callback

	remove: (callback) ->
		@api._send 'DELETE', '/account', {auth: 'key'}, callback

	add_key: (name, options, callback) ->
		args = Array::slice.call(arguments)
		callback = args.pop()
		name = args.shift()
		options = args.pop()

		@api._send 'POST', '/account/key/' + name, {body: options, auth: 'key'}, callback

	remove_key: (name, callback) ->
		@api._send 'DELETE', '/account/key/' + name, {auth: 'key'}, callback


# Module Definitions
# Node
if module?
	module.exports = AuthIOAccount
# AMD/RequireJS
if define?
	define 'authioClient-account', [], -> AuthIOAccount

if angular?
	angular.module('authio.client.account', []).factory 'AuthIOAccount', -> AuthIOAccount
